function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    for(let val in obj){
        
            obj[val] = cb(obj[val]);
        
    }
    return obj
}

module.exports = mapObject;