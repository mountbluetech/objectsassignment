function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    let result = [];
    for(let element in obj){
        result.push([element, obj[element]]);
    }
    return result;
}

module.exports = pairs;