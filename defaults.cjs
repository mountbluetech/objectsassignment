function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
    for(let ele in defaultProps) {
        if(ele in obj) {
            obj[ele] = defaultProps[ele];
        }
    }
    return obj;
}

module.exports = defaults;